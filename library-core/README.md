Live Data Elements-Core
===============

This module contains core elements.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Alive_elements/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Alive_elements/_latestVersion)

### Gradle ###

    compile "universum.studios.android:live-data-elements-core:${DESIRED_VERSION}@aar"

_depends on:_
[...]({TODO})

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [...]({TODO})